-- global variables
capmode = false
mbmode = true
tpmodes = {'dualwield0', 'dualwield20', 'dualwield40', 'dt', 'evasion', 'magicevasion', 'accuracy', 'treasure'}
tpmodeindex = 1;
queuedset = {}
isqueued = false

function get_sets()
    sets.idle = {}                  -- Leave this empty
    sets.precast = {}               -- Leave this empty    
    sets.midcast = {}               -- Leave this empty    
    sets.aftercast = {}             -- Leave this empty
    sets.engaged = {}				-- Leave this empty
    sets.ws = {}					-- Leave this empty
    sets.ja = {}					-- Leave this empty
    sets.special = {}				-- Leave this empty
    sets.weapons = {}				-- Leave this empty

    -- JSE capes
    andartia={}
    andartia.stpdex = { name="Andartia's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','"Store TP"+10','Damage taken-5%',}}
    andartia.dadex = { name="Andartia's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','"Dbl.Atk."+10',}}
    andartia.fastcast = { name="Andartia's Mantle", augments={'HP+60','Eva.+20 /Mag. Eva.+20','"Fast Cast"+10',}}
    andartia.elenin = { name="Andartia's Mantle", augments={'INT+20','Mag. Acc+20 /Mag. Dmg.+20','INT+10','"Mag.Atk.Bns."+10',}}
    andartia.wsdagi = { name="Andartia's Mantle", augments={'AGI+20','Accuracy+20 Attack+20','Weapon skill damage +10%',}}
    andartia.wsdstr = { name="Andartia's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}}
    andartia.wsddex = { name="Andartia's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','Weapon skill damage +10%',}}

    -- weapon sets
    sets.weapons.gokotaitauret = {
    	name="Gokotai / Tauret",
	    main="Gokotai",
    	sub="Tauret",
    }

    sets.weapons.gokotaiternion = {
    	name="Gokotai / Ternion Dagger +1",
	    main="Gokotai",
    	sub={ name="Ternion Dagger +1", augments={'Path: A',}},
    }

    sets.weapons.kikokuternion = {
    	name="Kikoku / Ternion Dagger +1",
	    main="Kikoku",
    	sub={ name="Ternion Dagger +1", augments={'Path: A',}},
    }

    sets.weapons.berylliumtachi = {
    	name="Beryllium Tachi / Tzacab Grip",
    	main="Beryllium Tachi",
    	sub="Tzacab Grip",
    }

    sets.weapons.kujakuternion = {
    	name="Kujaku / Ternion Dagger +1",
	    main="Kujaku",
    	sub={ name="Ternion Dagger +1", augments={'Path: A',}},
    }

    -- idle sets
    sets.idle.normal = {
	    ammo="White Tathlum",
	    head="Malignance Chapeau",
	    body="Hiza. Haramaki +2",
	    hands="Malignance Gloves",
	    legs="Mummu Kecks +2",
	    feet="Danzo Sune-Ate",
	    neck={ name="Ninja Nodowa +2", augments={'Path: A',}},
	    waist="Svelt. Gouriz +1",
	    left_ear="Infused Earring",
	    right_ear="Eabani Earring",
	    left_ring="Sheltered Ring",
	    right_ring="Defending Ring",
	    back=andartia.stpdex,
    }

    sets.idle.night = set_combine(sets.idle.normal, {
    	feet="Hachi. Kyahan +1"
    })

    -- precast sets
    sets.precast.fastcast = {
	    head={ name="Herculean Helm", augments={'"Fast Cast"+6','STR+10','Mag. Acc.+4','"Mag.Atk.Bns."+2',}},
	    body={ name="Taeon Tabard", augments={'Rng.Acc.+18 Rng.Atk.+18','"Fast Cast"+5','AGI+8',}},
	    hands="Sombra Mittens",
	    legs={ name="Herculean Trousers", augments={'"Mag.Atk.Bns."+24','"Fast Cast"+6','MND+9','Mag. Acc.+7',}},
	    feet={ name="Herculean Boots", augments={'"Fast Cast"+6','"Mag.Atk.Bns."+15',}},
	    neck="Orunmila's Torque",
	    waist="Svelt. Gouriz +1",
	    left_ear="Enchntr. Earring +1",
	    right_ear="Loquac. Earring",
	    left_ring="Weather. Ring",
	    right_ring="Kishar Ring",
	    back=andartia.fastcast,
    }

    sets.precast.utsusemi = set_combine(sets.precast.fastcast, {
	    neck="Magoraga Beads",
    })

    -- midcast sets
    sets.midcast.utsusemi = set_combine(sets.precast.fastcast, {
	    feet="Hattori Kyahan +2",
    })

    sets.midcast.migawari = sets.precast.fastcast

    sets.midcast.elemental = {
	    ammo={ name="Ghastly Tathlum +1", augments={'Path: A',}},
	    head={ name="Mochi. Hatsuburi +3", augments={'Enhances "Yonin" and "Innin" effect',}},
    	body={ name="Samnuha Coat", augments={'Mag. Acc.+10','"Mag.Atk.Bns."+9','"Fast Cast"+2',}},
	    hands="Hattori Tekko +2",
	    legs="Gyve Trousers",
	    feet={ name="Mochi. Kyahan +1", augments={'Enh. Ninj. Mag. Acc/Cast Time Red.',}},
	    neck="Sibyl Scarf",
	    waist="Eschan Stone",
	    left_ear="Friomisi Earring",
	    right_ear="Hecate's Earring",
	    left_ring={ name="Metamor. Ring +1", augments={'Path: A',}},
	    right_ring="Shiva Ring +1",
	    back=andartia.elenin,
    }

    sets.midcast.elemental.mb = set_combine(sets.midcast.elemental, {
	    right_ring="Mujin Band",
    })

	sets.midcast.enfeebling = {
	    ammo="Hydrocera",
	    head="Hachiya Hatsu. +3",
	    body="Hattori Ningi +2",
	    hands="Hattori Tekko +2",
	    legs="Mummu Kecks +2",
	    feet="Hattori Kyahan +2",
	    neck="Sanctity Necklace",
	    waist="Eschan Stone",
	    left_ear="Hnoss Earring",
	    right_ear="Gwati Earring",
	    left_ring={ name="Metamor. Ring +1", augments={'Path: A',}},
	    right_ring="Stikini Ring",
	    back=andartia.elenin,
	}

	sets.midcast.enmity = {

	}

	-- engaged sets
    sets.engaged.dualwield0 = {
		ammo="Date Shuriken",
		head="Malignance Chapeau",
		body="Malignance Tabard",
		hands="Malignance Gloves",
		legs="Ken. Hakama",
		feet="Malignance Boots",
		neck={ name="Ninja Nodowa +2", augments={'Path: A',}},
		waist="Windbuffet Belt +1",
		left_ear="Cessance Earring",
		right_ear="Brutal Earring",
		left_ring="Epona's Ring",
		right_ring="Chirich Ring",
	    back=andartia.stpdex,
    }

    sets.engaged.dualwield20 = {
	    ammo="Date Shuriken",
		head="Malignance Chapeau",
		body="Malignance Tabard",
		hands="Malignance Gloves",
	    legs="Ken. Hakama",
		feet="Malignance Boots",
		neck={ name="Ninja Nodowa +2", augments={'Path: A',}},
	    waist="Patentia Sash",
	    left_ear="Suppanomimi",
	    right_ear="Eabani Earring",
	    left_ring="Epona's Ring",
	    right_ring="Chirich Ring",
	    back=andartia.stpdex,
    }

    sets.engaged.dualwield40 = {
	    ammo="Date Shuriken",
      	head="Ptica Headgear",
	    body="Hachi. Chain. +1",
	    hands="Ken. Tekko",
	    legs="Sombra Tights",
	    feet="Hiza. Sune-Ate +2",
		neck={ name="Ninja Nodowa +2", augments={'Path: A',}},
	    waist="Patentia Sash",
	    left_ear="Suppanomimi",
	    right_ear="Eabani Earring",
	    left_ring="Epona's Ring",
	    right_ring="Chirich Ring",
	    back=andartia.stpdex,
    }

	sets.engaged.dt = set_combine(sets.engaged.dualwield0, {
		right_ring="Defending Ring",
		neck="Elite royal collar",
	})

	sets.engaged.evasion = {
	    ammo="Date Shuriken",
	    head="Hizamaru Somen +1",
	    body="Hiza. Haramaki +2",
	    hands="Hizamaru Kote +1",
	    legs="Hiza. Hizayoroi +2",
	    feet="Hiza. Sune-Ate +2",
	    neck="Bathy Choker +1",
	    waist="Svelt. Gouriz +1",
	    left_ear="Eabani Earring",
	    right_ear="Infused Earring",
	    left_ring="Hizamaru Ring",
	    right_ring="Chirich Ring",
	    back=andartia.stpdex,
	}

	sets.engaged.magicevasion = {
		ammo="Date Shuriken",
	    head="Hizamaru Somen +1",
	    body="Hiza. Haramaki +2",
	    hands="Hizamaru Kote +1",
	    legs="Gyve Trousers",
	    feet="Hiza. Sune-Ate +2",
	    neck="Invidia Torque",
	    waist="Patentia Sash",
	    left_ear="Suppanomimi",
	    right_ear="Hades Earring +1",
	    left_ring="Epona's Ring",
	    right_ring="Chirich Ring",
	    back=andartia.fastcast,
	}


	sets.engaged.accuracy = {

	}

	sets.engaged.treasure = set_combine(sets.engaged.dualwield0, {
		ammo="Per. Lucky Egg",
		waist="Chaac Belt",
	})

	-- job ability sets
	sets.ja['Mijin Gakure'] = {
		legs={ name="Mochi. Hakama +3", augments={'Enhances "Mijin Gakure" effect',}},
	}
	sets.ja['Provoke'] = sets.midcast.enmity
	sets.ja['Warcry'] = sets.midcast.enmity

	-- weapon skill sets
	sets.ws.debuff = {
	    ammo="Voluspa Tathlum",
	    head="Malignance Chapeau",
	    body="Malignance Tabard",
	    hands="Malignance Gloves",
	    legs="Mummu Kecks +2",
	    feet="Hattori Kyahan +2",
	    neck="Sanctity Necklace",
	    waist="Eschan Stone",
	    left_ear="Mache Earring +1",
	    right_ear="Gwati Earring",
	    left_ring="Mummu Ring",
	    right_ring="Vertigo Ring",
	    back=andartia.wsdstr
	}

	sets.ws.hybrid = {
	    ammo={ name="Seeth. Bomblet +1", augments={'Path: A',}},
	    head="Hachiya Hatsu. +3",
	    body="Hiza. Haramaki +2",
	    hands="Hattori Tekko +2",
    	legs={ name="Mochi. Hakama +3", augments={'Enhances "Mijin Gakure" effect',}},
	    feet="Hattori Kyahan +2",
	    neck="Fotia Gorget",
	    waist="Sailfi Belt +1",
	    left_ear={ name="Lugra Earring +1", augments={'Path: A',}},
	    right_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
	    left_ring="Apate Ring",
	    right_ring="Epona's Ring",
	    back=andartia.wsdstr,  
	}

	sets.ws.magic = {
	    ammo={ name="Ghastly Tathlum +1", augments={'Path: A',}},
		head={ name="Mochi. Hatsuburi +3", augments={'Enhances "Yonin" and "Innin" effect',}},    	
		body={ name="Herculean Vest", augments={'"Mag.Atk.Bns."+24','Crit. hit damage +2%','INT+2','Mag. Acc.+5',}},
    	hands={ name="Herculean Gloves", augments={'"Mag.Atk.Bns."+25','Weapon skill damage +4%','STR+5',}},	    
    	legs={ name="Herculean Trousers", augments={'"Mag.Atk.Bns."+24','"Fast Cast"+6','MND+9','Mag. Acc.+7',}},
    	feet={ name="Herculean Boots", augments={'Mag. Acc.+11 "Mag.Atk.Bns."+11','Magic burst dmg.+5%','STR+5','"Mag.Atk.Bns."+13',}},
	    waist="Eschan Stone",
	    left_ear="Friomisi Earring",
	    right_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
	    left_ring="Shiva Ring +1",
	    right_ring="Shiva Ring +1",
	    back=andartia.elenin,
	}

    sets.ws['Blade: Ku'] = {
	    ammo="Voluspa Tathlum",
	    head="Hachiya Hatsu. +3",
	    body="Hattori Ningi +2",
	    hands="Hattori Tekko +2",
    	legs={ name="Mochi. Hakama +3", augments={'Enhances "Mijin Gakure" effect',}},
	    feet="Hattori Kyahan +2",
	    neck="Fotia Gorget",
	    waist="Fotia Belt",
	    left_ear="Brutal Earring",
	    right_ear={ name="Lugra Earring +1", augments={'Path: A',}},
	    left_ring="Apate Ring",
	    right_ring="Epona's Ring",
	    back=andartia.wsdstr,    
	}

	sets.ws['Blade: Shun'] = {
	    ammo="Voluspa Tathlum",
	    head="Hachiya Hatsu. +3",
	    body="Hattori Ningi +2",
	    hands="Hattori Tekko +2",
    	legs={ name="Mochi. Hakama +3", augments={'Enhances "Mijin Gakure" effect',}},
	    feet="Hattori Kyahan +2",
	    neck="Fotia Gorget",
	    waist="Fotia Belt",
	    left_ear={ name="Lugra Earring +1", augments={'Path: A',}},
	    right_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
	    left_ring="Apate Ring",
	    right_ring="Epona's Ring",
	    back=andartia.dadex
	}

	sets.ws['Blade: Hi'] = {
	    ammo="Yetshila",
	    head="Hachiya Hatsu. +3",
	    body="Hattori Ningi +2",
	    hands="Mummu Wrists +2",
	    legs="Mummu Kecks +2",
	    feet="Mummu Gamash. +2",
	    neck={ name="Ninja Nodowa +2", augments={'Path: A',}},
	    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
	    left_ear={ name="Lugra Earring +1", augments={'Path: A',}},
	    right_ear="Odr Earring",
	    left_ring="Begrudging Ring",
	    right_ring="Mummu Ring",
	    back=andartia.wsdagi,
	}

	sets.ws['Blade: Ten'] = {
	    ammo="Voluspa Tathlum",
	    head="Hachiya Hatsu. +3",
	    body="Hattori Ningi +2",
	    hands="Hattori Tekko +2",
    	legs={ name="Mochi. Hakama +3", augments={'Enhances "Mijin Gakure" effect',}},
	    feet="Hattori Kyahan +2",
		neck="Rep. Plat. Medal",
	    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
	    left_ear={ name="Lugra Earring +1", augments={'Path: A',}},
	    right_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
	    left_ring="Apate Ring",
	    right_ring="Rajas Ring",
	    back=andartia.wsdstr
	}

	sets.ws['Blade: Metsu'] = {
	    ammo="Voluspa Tathlum",
	    head="Hachiya Hatsu. +3",
	    body="Hattori Ningi +2",
	    hands="Hattori Tekko +2",
    	legs={ name="Mochi. Hakama +3", augments={'Enhances "Mijin Gakure" effect',}},
	    feet="Hattori Kyahan +2",
		neck="Rep. Plat. Medal",
	    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
	    left_ear="Odr Earring",
	    right_ear={ name="Lugra Earring +1", augments={'Path: A',}},
	    left_ring="Rajas Ring",
	    right_ring="Apate Ring",
	    back=andartia.wsddex
	}

	sets.ws['Blade: Kamu'] = {
	    ammo="Voluspa Tathlum",
	    head="Hachiya Hatsu. +3",
	    body="Hattori Ningi +2",
	    hands="Hattori Tekko +2",
    	legs={ name="Mochi. Hakama +3", augments={'Enhances "Mijin Gakure" effect',}},
	    feet="Hattori Kyahan +2",
	    neck={ name="Ninja Nodowa +2", augments={'Path: A',}},
	    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
	    left_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
	    right_ear={ name="Lugra Earring +1", augments={'Path: A',}},
	    left_ring="Apate Ring",
	    right_ring="Rajas Ring",
	    back=andartia.wsdstr
	}

	sets.ws['Blade: Ei'] = sets.ws.magic
	sets.ws['Blade: Yu'] = sets.ws.magic
	sets.ws['Aeolian Edge'] = sets.ws.magic

	sets.ws['Blade: Retsu'] = sets.ws.debuff
	sets.ws['Tachi: Ageha'] = sets.ws.debuff
	sets.ws['Tachi: Gekko'] = sets.ws.debuff

	sets.ws['Blade: Chi'] = sets.ws.hybrid
	sets.ws['Blade: Teki'] = sets.ws.hybrid
	sets.ws['Blade: To'] = sets.ws.hybrid

	sets.ws['Evisceration'] = {
	    ammo="Yetshila",
	    head="Hachiya Hatsu. +3",
	    body="Mummu Jacket +2",
	    hands="Mummu Wrists +2",
    	legs={ name="Mochi. Hakama +3", augments={'Enhances "Mijin Gakure" effect',}},
	    feet="Hattori Kyahan +2",
	    neck="Fotia Gorget",
	    waist="Fotia Belt",
	    left_ear="Brutal Earring",
	    right_ear="Odr Earring",
	    left_ring="Epona's Ring",
	    right_ring="Begrudging Ring",
	    back=andartia.dadex,
	}

    sets.special.capacitypoints = {
    	back="Aptitude Mantle +1"
    }
 
end
 
function precast(spell)
	if spell.english:startswith('Utsusemi') then
		equip(sets.precast.utsusemi)	
	elseif spell.type == "Ninjutsu" or spell.type == "WhiteMagic" or spell.type == "BlackMagic" or spell.type == "Trust" then
		equip (sets.precast.fastcast)
	end
end
 
function midcast(spell)
	if spell.type == "Ninjutsu" then
		midcast_nijutsu(spell)
	elseif spell.type == "WeaponSkill" then
		equip(sets.ws[spell.english])
	elseif spell.skill == "Enfeebling Magic" then
		equip(sets.midcast.enfeebling)
	end
end

function midcast_nijutsu(spell)
	if spell.english:startswith('Utsusemi') then
		equip(sets.midcast.utsusemi)
	elseif spell.english:startswith('Katon') 
		or spell.english:startswith('Hyoton') 
		or spell.english:startswith('Huton') 
		or spell.english:startswith('Doton') 
		or spell.english:startswith('Raiton')
		or spell.english:startswith('Suiton') then
		if mbmode then
			equip(sets.midcast.elemental.mb)
		else 
			equip(sets.midcast.elemental)
		end
	elseif spell.english:startswith('Aisha')
		or spell.english:startswith('Dokumori')
		or spell.english:startswith('Hojo')
		or spell.english:startswith('Jubaku')
		or spell.english:startswith('Kurayami')
		or spell.english:startswith('Yurin') then
		equip(sets.midcast.enfeebling)
	elseif spell.english:startswith('Migawari') then
		equip(sets.midcast.migawari)
	end
end

function aftercast(spell)
	handle_status_change()
	if spell.type == "WeaponSkill" and isqueued then
		equip(queuedset)
		isqueued = false
	end
end
 
function status_change(new,old)
	handle_status_change()
end

function handle_status_change()
	if player.status == "Engaged" then
		equip_tp_set()
	else
		equip_idle_set()
	end
	if capmode then 
		equip(sets.special.capacitypoints)
	end
end

function equip_tp_set()
	local tpmode = tpmodes[tpmodeindex]
	equip(sets.engaged[tpmode])
end

function equip_idle_set()
	if world.time >= (17*60) or world.time < (7*60) then
		equip(sets.idle.night)
	else
		equip(sets.idle.normal)
	end
end

function self_command(command)
	local commandArgs = command
	if #commandArgs:split(' ') >= 2 then
		commandArgs = T(commandArgs:split(' '))
		parse_command(commandArgs[1], commandArgs[2], commandArgs[3])
	else
		windower.add_to_chat(123, "Error: Commands require at least 2 parameters.")
	end
end

function parse_command(param1, param2, param3)
	if param1 == "toggle" then
		handle_toggle_command(param2)
	elseif param1 == "set" then
		handle_set_command(param2, param3)
	elseif param1 == "queue" then
		handle_queue_command(param2)
	else
		windower.add_to_chat(123, "Error: First parameter not recognized.")
	end
end

function handle_toggle_command(param2)
	if param2 == "capmode" then
		capmode = not capmode
		windower.add_to_chat(123, "Capacity Point Mode set to " .. tostring(capmode) .. ".")
	elseif param2 == "mbmode" then
		mbmode = not mbmode
		windower.add_to_chat(123, "Magic Burst Mode set to " .. tostring(mbmode) .. ".")
	else
		windower.add_to_chat(123, "Error: Second parameter not recognized.")
	end
end	

function handle_set_command(param2, param3)
	if param2 == "tpmode" then
		if param3 == "dualwield0" then
			tpmodeindex = 1
			windower.add_to_chat(123, "TP Mode set to DUAL WIELD 0")
		elseif param3 == "dualwield20" then
			tpmodeindex = 2
			windower.add_to_chat(123, "TP Mode set to DUAL WIELD 20")
		elseif param3 == "dualwield40" then
			tpmodeindex = 3
			windower.add_to_chat(123, "TP Mode set to DUAL WIELD 40")
		elseif param3 == "dt" then
			tpmodeindex = 4
			windower.add_to_chat(123, "TP Mode set to DT")
		elseif param3 == "evasion" then
			tpmodeindex = 5
			windower.add_to_chat(123, "TP Mode set to EVASION")
		elseif param3 == "magicevasion" then
			tpmodeindex = 6
			windower.add_to_chat(123, "TP Mode set to MAGIC EVASION")			
		elseif param3 == "accuracy" then
			tpmodeindex = 7
			windower.add_to_chat(123, "TP Mode set to ACCURACY")
		elseif param3 == "treasure" then
			tpmodeindex = 8
			windower.add_to_chat(123, "TP Mode set to TREASURE")
		else 
			windower.add_to_chat(123, "Error: Third parameter not recognized.")
		end
	else 
		windower.add_to_chat(123, "Error: Second parameter not recognized.")
	end
end

function handle_queue_command(param2)
	queuedset = sets.weapons[param2]
	isqueued = true
	windower.add_to_chat(123, "Weapon in queue: " .. queuedset.name)
end